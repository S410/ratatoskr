package main

import (
	"math/rand"
	"sync"
	"time"
	"unsafe"

	"golang.org/x/crypto/bcrypt"
)

const (
	DefaultTokenTTL = time.Hour

	randomAccessTokenLength = 64
	randomClientTokenLength = 64
)

type ClientToken string

type AccessToken struct {
	AccessToken string
	EOL         int64
}

// NewToken generates an AccessToken and, if not provided, a ClientToken
func NewToken(ttl time.Duration) AccessToken {
	if ttl == 0 {
		ttl = DefaultTokenTTL
	}

	return AccessToken{
		randomBase64(randomAccessTokenLength),
		time.Now().Add(ttl).Unix(),
	}
}

func (self AccessToken) IsExpired() bool {
	return time.Now().Unix() > self.EOL
}

type Account struct {
	lock        sync.RWMutex
	AccountName string
	Password    []byte
	ServerId    string
	Tokens      map[ClientToken]AccessToken
}

func (self *Account) GetName() string {
	self.lock.RLock()
	defer self.lock.RUnlock()

	return self.AccountName
}

func (self *Account) updateToken(clientToken ClientToken, ttl time.Duration) (AccessToken, ClientToken) {
	t := NewToken(ttl)

	if clientToken == "" {
		clientToken = randomClientToken()
	}

	self.Tokens[clientToken] = t

	return t, clientToken
}

// UpdateToken generates a new AccessToken and, if clientToken is proveded, updates AccessToken related to it.
// If clientToken is empty a new ClientToken is generated and a new token pair added to the account.
// If ttl is 0, default ttl duration is used.
func (self *Account) UpdateToken(clientToken string, ttl time.Duration) (AccessToken, ClientToken) {
	self.lock.Lock()
	defer self.lock.Unlock()

	return self.updateToken(ClientToken(clientToken), ttl)
}

func (self *Account) checkToken(accessToken, clientToken string) (mathes, expired bool) {
	for k, t := range self.Tokens {
		if accessToken == t.AccessToken && (clientToken == "" || ClientToken(clientToken) == k) {
			return true, t.IsExpired()
		}
	}

	return
}

func (self *Account) CheckToken(accessToken, clientToken string) (mathes, expired bool) {
	self.lock.RLock()
	defer self.lock.RUnlock()

	return self.checkToken(accessToken, clientToken)
}

func (self *Account) setPassword(s string) {
	self.Password = hashPassword(s)
}

func (self *Account) SetPassword(s string) {
	hash := hashPassword(s)

	self.lock.Lock()
	self.Password = hash
	self.lock.Unlock()
}

func (self *Account) checkPassword(s string) (matches bool) {
	return bcrypt.CompareHashAndPassword(self.Password, []byte(s)) == nil
}

func (self *Account) CheckPassword(s string) (matches bool) {
	self.lock.RLock()
	defer self.lock.RUnlock()

	return self.checkPassword(s)
}

func (self *Account) updateServerId(s string) {
	self.ServerId = s
}

func (self *Account) SetServerId(s string) {
	self.lock.Lock()
	defer self.lock.Unlock()

	self.ServerId = s
}

func (self *Account) GetServerId() string {
	self.lock.RLock()
	defer self.lock.RUnlock()

	return self.ServerId
}

func (self *Account) clearTokens() {
	self.Tokens = map[ClientToken]AccessToken{}
}

func (self *Account) ClearTokens() {
	self.lock.Lock()
	defer self.lock.Unlock()

	self.clearTokens()
}

func (self *Account) DeleteToken(clientToken string) {
	self.lock.Lock()
	defer self.lock.Unlock()

	delete(self.Tokens, ClientToken(clientToken))
}

// func (self *Account) invalidateToken(accessToken string) {
// }

func hashPassword(s string) []byte {
	hash, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	return hash
}

// randomBase64 return a random string made up of base64 characters.
func randomBase64(len int) string {
	b := make([]byte, len)

	for i := range b {
		n := rand.Uint32() & 63
		switch {
		case n < 26:
			b[i] = byte(n + 'A')
		case n < 52:
			b[i] = byte(n - 26 + 'a')
		case n == 62:
			b[i] = '+'
		case n == 63:
			b[i] = '/'
		default:
			b[i] = byte(n - 52 + '0')
		}
	}

	return *(*string)(unsafe.Pointer(&b))
}

func randomClientToken() ClientToken {
	return ClientToken(randomBase64(randomClientTokenLength))
}
