package main

import (
	"encoding/json"
	"net/http"
)

type YggError struct {
	status int
	json   []byte
}

func (self YggError) RespondTo(w http.ResponseWriter) {
	w.WriteHeader(self.status)
	w.Write(self.json)
}

func (self *YggError) String() string {
	return string(self.json)
}

func (self *YggError) Status() int {
	return self.status
}

var (
	yggErrorMethodNotAllowed = yggFormatError(
		http.StatusMethodNotAllowed,
		"Method Not Allowed",
		"The method specified in the request is not allowed for the resource identified by the request URI",
		"",
	)

	yggErrorUnsupportedMediaType = yggFormatError(
		http.StatusUnsupportedMediaType,
		"Unsupported Media Type",
		"The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method",
		"",
	)

	yggErrorForbidden = yggFormatError(
		http.StatusForbidden,
		"ForbiddenOperationException",
		"Forbidden",
		"",
	)

	yggErrorNotFound = yggFormatError(
		http.StatusNotFound,
		"Not Found",
		"The server has not found anything matching the request URI",
		"",
	)

	yggErrorInternalServerError = yggFormatError(
		http.StatusInternalServerError,
		"InternalServerError",
		"Internal Server Error",
		"",
	)

	yggErrorInvalidCredentials = yggFormatError(
		http.StatusForbidden,
		"ForbiddenOperationException",
		"Invalid credentials.",
		"",
	)

	yggErrorBadRequest = yggFormatError(
		http.StatusBadRequest,
		"ResourceException",
		"Bad Request (400) - The request could not be understood by the server due to malformed syntax",
		"",
	)
)

func yggFormatError(status int, err, errorMessage, cause string) *YggError {
	m := map[string]string{
		"error":        err,
		"errorMessage": errorMessage,
	}

	if cause != "" {
		m["cause"] = cause
	}

	json, _ := json.Marshal(m)

	return &YggError{
		status,
		json,
	}
}
