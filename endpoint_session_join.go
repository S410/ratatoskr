package main

import (
	"net/http"
)

func init() {
	handleEndpointFunc("/session/minecraft/join", handlerSessionJoin, "POST")
}

func handlerSessionJoin(w http.ResponseWriter, r *http.Request) {
	p := struct {
		_AccessToken
		_ServerId
	}{}

	err := readAndUnmarshal(r.Body, &p)
	if err != nil {
		err.RespondTo(w)

		return
	}

	acc := getAccountByToken(p.AccessToken, "", true)
	if acc == nil {
		w.WriteHeader(http.StatusForbidden)

		return
	}

	acc.SetServerId(p.ServerId)

	w.WriteHeader(http.StatusNoContent)

	saveAccDb()
}
