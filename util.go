package main

import (
	"encoding/hex"
	"encoding/json"
	"io"
	"net/http"
	"os"
	"strings"

	"golang.org/x/crypto/sha3"
)

func envOr(key, def string) string {
	v, ok := os.LookupEnv(key)
	if ok {
		return v
	}

	return def
}

// unmarshal is a wrapper around the standard funciton that, if an error occurs,
// formats it to Yggdrasil format ready to be returned to the client
func unmarshal(data []byte, j interface{}) *YggError {
	err := json.Unmarshal(data, &j)

	if err != nil {
		return yggFormatError(http.StatusBadRequest, "JsonParseException", err.Error(), "")
	}

	return nil
}

func readAndUnmarshal(r io.Reader, j interface{}) *YggError {
	b, err := io.ReadAll(r)
	if err != nil {
		return yggErrorInternalServerError
	}

	return unmarshal(b, j)
}

// marshalAndSend attemts to marshal a value and writes either the result
// or InternalServerError in Yggrasil format to the privided ResponseWriter.
func marshalAndSend(v interface{}, w http.ResponseWriter) {
	b, err := json.Marshal(v)
	if err != nil {
		yggErrorInternalServerError.RespondTo(w)

		return
	}

	w.Write(b)
}

type HashUUID [16]byte

func NewHashUUID(s string) HashUUID {
	h := HashUUID{}

	sha3.ShakeSum128(h[:], []byte(strings.ToLower(s)))

	return h
}

func (self HashUUID) Dashed() string {
	b := [36]byte{
		8:      '-',
		12 + 1: '-',
		16 + 2: '-',
		20 + 3: '-',
	}

	hex.Encode(b[:8], self[:4])
	hex.Encode(b[8+1:12+1], self[4:6])
	hex.Encode(b[12+2:16+2], self[6:8])
	hex.Encode(b[16+3:20+3], self[8:10])
	hex.Encode(b[20+4:32+4], self[10:16])

	return string(b[:])
}

func (self HashUUID) Dashless() string {
	return hex.EncodeToString(self[:])
}

// func randomUuid() string {
// 	return fmt.Sprintf(
// 		"%08x-%04x-4%03x-%04x-%012x",
// 		rand.Uint64()%0xFFFFFFFF,
// 		rand.Uint64()%0xFFFF,
// 		rand.Uint64()%0xFFF,
// 		rand.Uint64()%0xFFFF&0x3FFF|0x8000,
// 		rand.Uint64()%0xFFFFFFFFFFFF,
// 	)
// }

// func randomUuidDashless() string {
// 	return fmt.Sprintf(
// 		"%08x%04x4%03x%04x%012x",
// 		rand.Uint64()%0xFFFFFFFF,
// 		rand.Uint64()%0xFFFF,
// 		rand.Uint64()%0xFFF,
// 		rand.Uint64()%0xFFFF&0x3FFF|0x8000,
// 		rand.Uint64()%0xFFFFFFFFFFFF,
// 	)
// }

// func twosComplement(p []byte) []byte {
// 	carry := true
// 	for i := len(p) - 1; i >= 0; i-- {
// 		p[i] = byte(^p[i])
// 		if carry {
// 			carry = p[i] == 0xff
// 			p[i]++
// 		}
// 	}
// 	return p
// }
