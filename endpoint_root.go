package main

import (
	"net/http"
)

func init() {
	handleEndpointFunc("/", handlerRoot, "GET")
}

func handlerRoot(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"Status":"OK"}`))
}
