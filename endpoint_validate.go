package main

import (
	"net/http"
)

func init() {
	handleEndpointFunc("/validate", handlerValidate, "POST")
}

func handlerValidate(w http.ResponseWriter, r *http.Request) {
	p := TokenPair{}

	err := readAndUnmarshal(r.Body, &p)
	if err != nil {
		err.RespondTo(w)

		return
	}

	if getAccountByToken(p.AccessToken, p.ClientToken, true) != nil {
		w.WriteHeader(http.StatusNoContent)
	} else {
		w.WriteHeader(http.StatusForbidden)
	}
}
