package main

import (
	"net/http"
)

func init() {
	handleEndpointFunc("/invalidate", handlerInvalidate, "POST")
}

func handlerInvalidate(w http.ResponseWriter, r *http.Request) {
	p := TokenPair{}

	readAndUnmarshal(r.Body, &p)

	acc := getAccountByToken(p.AccessToken, p.ClientToken, false)
	if acc == nil {
		yggErrorBadRequest.RespondTo(w)

		return
	}

	if acc != nil {
		acc.DeleteToken(p.ClientToken)

		saveAccDb()
	}

	w.WriteHeader(http.StatusNoContent)
}
