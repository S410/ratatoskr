package main

import (
	"net/http"
)

func init() {
	handleEndpointFunc("/register", handlerRegister, "POST")
}

var (
	errorAccountExistsError = yggFormatError(
		http.StatusBadRequest,
		"AccountAlreadyExists",
		accErrorAccountAlreadyExists.Error(),
		"",
	)

	errorPasswordTooShort = yggFormatError(
		http.StatusBadRequest,
		"PasswordTooShort",
		"Provided password is too short",
		"",
	)
)

func handlerRegister(w http.ResponseWriter, r *http.Request) {
	p := Credentials{}

	yerr := readAndUnmarshal(r.Body, &p)
	if yerr != nil {
		yerr.RespondTo(w)

		return
	}

	if len(p.Password) < 6 {
		errorPasswordTooShort.RespondTo(w)

		return
	}

	_, err := addAccount(p.Username, p.Password)
	if err != nil {
		errorAccountExistsError.RespondTo(w)

		return
	}

	w.WriteHeader(http.StatusNoContent)

	saveAccDb()
}
