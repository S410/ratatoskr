package main

import (
	"net/http"
)

func init() {
	handleEndpointFunc("/signout", handlerInvalidate, "POST")
}

func handlerSignout(w http.ResponseWriter, r *http.Request) {
	p := Credentials{}

	yerr := readAndUnmarshal(r.Body, &p)
	if yerr != nil {
		yerr.RespondTo(w)

		return
	}

	acc := getAccountByName(p.Username)

	if acc != nil && acc.CheckPassword(p.Password) {
		acc.ClearTokens()

		saveAccDb()
	}

	w.WriteHeader(http.StatusNoContent)
}
