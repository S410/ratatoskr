package main

import (
	"encoding/json"
	"errors"
	"os"
	"strings"
	"sync"
)

// This implementation is stupid, but will work for testing

var (
	accountDbPath string
	accDbLock     = sync.RWMutex{}
	accounts      = []*Account{}
)

var (
	accErrorAccountAlreadyExists  = errors.New("Account already exists")
	accErrorAccountEmptyName      = errors.New("Account name cannot be empty")
	accErrorAccountEmptyShortPass = errors.New("Account password is too short")

	accErrorEmptyDbPath = errors.New("Database path is empty")
)

func unsafe_getAccountByName(name string) *Account {
	for i := range accounts {
		a := accounts[i]

		if strings.EqualFold(a.AccountName, name) {
			return a
		}
	}

	return nil
}

// GetAccountByName returns an account that has matching name (case insensitive).
// If no account is found nil is returned.
func getAccountByName(name string) *Account {
	accDbLock.RLock()
	defer accDbLock.RUnlock()

	return unsafe_getAccountByName(name)
}

func unsafe_getAccountByToken(accessToken, clientToken string, honorExpiration bool) *Account {
	for _, a := range accounts {
		ok, expired := a.CheckToken(accessToken, clientToken)
		if ok && (!honorExpiration || !expired) {
			return a
		}
	}

	return nil
}

// GetAccountByToken returns an account that has matching accessToken or, if clientToken is provided,
// matching pair of tokens. If honorExpiration is true, an account will be returned only if the token is not expired.
// If no match is found nil is returned.
func getAccountByToken(accessToken, clientToken string, honorExpiration bool) *Account {
	accDbLock.RLock()
	defer accDbLock.RUnlock()

	return unsafe_getAccountByToken(accessToken, clientToken, honorExpiration)
}

func addAccount(accountName, password string) (*Account, error) {
	accDbLock.Lock()
	defer accDbLock.Unlock()

	test := unsafe_getAccountByName(accountName)
	if test != nil {
		return nil, accErrorAccountAlreadyExists
	}

	acc := &Account{
		AccountName: accountName,
		Tokens:      map[ClientToken]AccessToken{},
	}

	acc.SetPassword(password)

	accounts = append(accounts, acc)

	return acc, nil
}

func unsafe_saveAccDb() error {
	b, err := json.MarshalIndent(
		&accounts,
		"", "\t",
	)

	if err != nil {
		return err
	}

	return os.WriteFile(accountDbPath, b, 0666)
}

func saveAccDb() error {
	accDbLock.Lock()
	defer accDbLock.Unlock()

	return unsafe_saveAccDb()
}

func unsafe_loadAccDb() error {
	b, _ := os.ReadFile(accountDbPath)

	accounts = nil
	err := json.Unmarshal(b, &accounts)
	if err != nil {
		accounts = nil
	}

	return err
}

func loadAccDb() error {
	accDbLock.Lock()
	defer accDbLock.Unlock()

	return unsafe_loadAccDb()
}
