package main

import (
	"net/http"
)

func init() {
	handleEndpointFunc("/refresh", handlerRefresh, "POST")
}

func handlerRefresh(w http.ResponseWriter, r *http.Request) {
	p := struct {
		TokenPair
		_RequestUser
	}{}

	e := readAndUnmarshal(r.Body, &p)
	if e != nil {
		errorLog.Println(e.String())
		e.RespondTo(w)

		return
	}

	acc := getAccountByToken(p.AccessToken, p.ClientToken, false)
	if acc == nil {
		yggErrorForbidden.RespondTo(w)

		return
	}

	at, ct := acc.UpdateToken(p.ClientToken, 0)
	prof := GetProfile(acc.GetName(), ownHostname)

	resp := map[string]interface{}{
		"accessToken":     at,
		"clientToken":     ct,
		"selectedProfile": prof,
	}

	if p.RequestUser {
		resp["user"] = map[string]interface{}{
			"id":         prof.Id,
			"properties": []string{},
		}
	}

	infoLog.Println(resp)

	marshalAndSend(resp, w)

	saveAccDb()
}
