package main

import (
	"flag"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"time"
)

var (
	infoLog  = log.New(os.Stdout, "", log.Ldate|log.Ltime)
	errorLog = log.New(os.Stderr, "ERROR:", log.Ldate|log.Ltime)
)

var (
	ownHostname = ""

	serveMux = &http.ServeMux{}

	server = &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		IdleTimeout:  15 * time.Second,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			h, p := serveMux.Handler(r)

			// Use non-default NotFound handler
			if p == "" {
				yggErrorNotFound.RespondTo(w)
				return
			}

			h.ServeHTTP(w, r)
		}),
	}

	client = &http.Client{
		Timeout: time.Second * 30,
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout: 5 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 5 * time.Second,
		},
	}
)

func methodFilter(f http.HandlerFunc, method string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch {
		case r.Method != method:
			yggErrorMethodNotAllowed.RespondTo(w)
		default:
			f(w, r)
		}
	}
}

func handleEndpointFunc(pattern string, handler http.HandlerFunc, strictMethod string) {
	if strictMethod != "" {
		handler = methodFilter(handler, strictMethod)
	}

	serveMux.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		infoLog.Println(r.Method, r.URL.String())

		handler.ServeHTTP(w, r)
	})
}

func main() {
	var (
		cert string
		key  string

		hostname string
	)

	flag.StringVar(&server.Addr, "a", "localhost:8080", "Address")
	flag.StringVar(&hostname, "h", "", "Hostname. Can be derived from address.")
	flag.StringVar(&cert, "c", "", "Certificate file")
	flag.StringVar(&key, "k", "", "Key file")
	flag.StringVar(&accountDbPath, "d", "db.json", `"""database"""`)

	flag.Parse()

	a := server.Addr

	u, err := url.Parse("meaninglessprotocol://" + a)
	if err != nil {
		panic(err)
	}

	if hostname != "" {
		ownHostname = hostname
	} else {
		ownHostname = u.Hostname()
	}

	if ownHostname == "" {
		errorLog.Fatalln("Hostname is empty")
	}

	loadAccDb()
	err = saveAccDb()
	if err != nil {
		panic(err)
	}

	log.Printf("Serving at %s\n", server.Addr)

	if cert == "" || key == "" {
		err = server.ListenAndServe()
	} else {
		err = server.ListenAndServeTLS(cert, key)
	}

	if err != nil {
		log.Fatal(err)
	}
}
