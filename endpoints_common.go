package main

// type Property struct {
// 	Name  string `json:"name"`
// 	Value string `json:"value"`
// }

type Profile struct {
	Name string `json:"name"` // Username
	Id   string `json:"id"`   // UUID
}

// GetProfile returns a Profile struct where Name is username@hostname and Id is a dashless uuid derived from the name
func GetProfile(username, hostname string) Profile {
	name := username + "@" + hostname

	return Profile{
		Name: name,
		Id:   NewHashUUID(name).Dashless(),
	}
}

// Common pairs
type Credentials struct {
	_Username
	_Password
}

type TokenPair struct {
	_AccessToken
	_ClientToken
}

// Common fields
type _Username struct {
	Username string `json:"username"`
}

type _Password struct {
	Password string `json:"password"`
}

type _AccessToken struct {
	AccessToken string `json:"accessToken"`
}

type _ClientToken struct {
	ClientToken string `json:"clientToken"`
}

type _RequestUser struct {
	RequestUser bool `json:"requestUser"`
}

type _ServerId struct {
	ServerId string `json:"serverId"`
}
