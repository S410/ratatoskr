package main

import (
	"net/http"
)

func init() {
	handleEndpointFunc("/authenticate", handlerAuthenticate, "POST")
}

func handlerAuthenticate(w http.ResponseWriter, r *http.Request) {
	ap := struct {
		// Agent    struct { ... }
		Credentials
		_ClientToken
		RequestUser bool `json:"requestUser"`
	}{}

	e := readAndUnmarshal(r.Body, &ap)
	if e != nil {
		errorLog.Println(e.String())
		e.RespondTo(w)

		return
	}

	acc := getAccountByName(ap.Username)

	infoLog.Println(acc.GetName(), ap.Password, acc.CheckPassword(ap.Password))

	if acc == nil || !acc.CheckPassword(ap.Password) {
		yggErrorInvalidCredentials.RespondTo(w)

		return
	}

	if ap.ClientToken == "" {
		acc.ClearTokens()
	}

	at, ct := acc.UpdateToken(ap.ClientToken, 0)

	p := GetProfile(acc.GetName(), ownHostname)

	ar := struct {
		// User struct {
		// 	Username   string       `json:"username"`
		// 	Properties []Properties `json:"properties"`
		// 	Id         string       `json:"id"`
		// } `json:"user"`
		TokenPair
		AvailableProfiles [1]Profile `json:"availableProfiles"`
		SelectedProfile   Profile    `json:"selectedProfile"`
	}{}

	ar.AvailableProfiles = [1]Profile{p}
	ar.SelectedProfile = p
	ar.AccessToken = at.AccessToken
	ar.ClientToken = string(ct)

	marshalAndSend(ar, w)

	saveAccDb()
}
