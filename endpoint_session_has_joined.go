package main

import (
	"errors"
	"net/http"
	"net/url"
)

const (
	canonicalHasJoinedPath = "/session/minecraft/hasJoined"
)

var (
	errorNoName = errors.New("Username cannot be blank")

	canonicalHasJoinedUrl = url.URL{}
)

func init() {
	handleEndpointFunc(canonicalHasJoinedPath, handlerSessionHasJoined, "GET")

	u, err := url.Parse(envOr(
		"HAS_JOINED_URL",
		"sessionserver.mojang.com"+canonicalHasJoinedPath,
	))

	if err != nil {
		panic(err)
	}

	path := u.Path
	if path == "" {
		path = canonicalHasJoinedPath
	}

	canonicalHasJoinedUrl = url.URL{
		Scheme: "https",
		Host:   u.Host,
		Path:   path,
	}

	infoLog.Println("canonicalHasJoinedUrl =", canonicalHasJoinedUrl.String())
}

type hasJoinedPayload struct {
	Id         string        `json:"id"`
	Name       string        `json:"name"`
	Properties []interface{} `json:"properties"`
}

// splitUsername splits username at the last @ and returns the name and the host separately.
// If no @ is present the name is returned as is and the host value is empty.
// If the name is empty an error is returned.
func splitUsername(username string) (name string, host string, err error) {
	for i := len(username) - 1; i > 0; i-- {
		if username[i] == '@' {
			host = username[i+1:]
			username = username[:i]

			break
		}
	}

	if username == "" {
		return "", "", errorNoName
	}

	return username, host, nil
}

func hasJoinedOnHost(host, username, serverId, clientIp string) (payload hasJoinedPayload, ok bool) {
	u := canonicalHasJoinedUrl // Copy

	// If not Mojang
	if host != "" {
		u.Host = host
	}

	// Rebuilding query from args instead of re-using the original url.Values object
	// to strip any non-canonical values.
	q := u.Query()
	q.Add("username", username)
	q.Add("serverId", serverId)
	q.Add("ip", clientIp)
	u.RawQuery = q.Encode()

	r, err := client.Get(u.String())
	if err != nil || r.StatusCode != http.StatusOK {

		return
	}

	yggErr := readAndUnmarshal(r.Body, &payload)

	if yggErr != nil {
		return hasJoinedPayload{}, false
	}

	return payload, true
}

func handlerSessionHasJoined(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	username := q.Get("username")
	serverId := q.Get("serverId")
	clientIp := q.Get("ip")

	if username == "" || serverId == "" {
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	name, host, err := splitUsername(username)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	payload := hasJoinedPayload{
		Properties: []interface{}{},
	}

	if host == ownHostname {
		acc := getAccountByName(name)
		if acc == nil || acc.GetServerId() != serverId {
			w.WriteHeader(http.StatusNoContent)

			return
		}

		payload.Name = acc.GetName() + "@primary"
	} else {
		var ok bool

		payload, ok = hasJoinedOnHost(
			host, username, serverId, clientIp,
		)

		if !ok {
			w.WriteHeader(http.StatusNoContent)

			return
		}

		payload.Name = username
	}

	// The UUID is (re-)calculated only if the UUID wasn't received from Mojang (blank host == Mojang).
	// UUID received from 3rd party auth servers cannot be trusted.
	if host != "" {
		payload.Id = NewHashUUID(payload.Name).Dashless()
	}

	infoLog.Println(payload)

	marshalAndSend(
		payload,
		w,
	)
}
